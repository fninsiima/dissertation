# dissertation

This repository contains the code, data, and documentation for the techniques explored for automating the segmentation of necrotized regions in cassava root images.

The published paper "Automating the segmentation of necrotized regions in cassava root images" can be found <a href = "https://csce.ucmss.com/cr/books/2018/LFS/CSREA2018/IPC3638.pdf">here</a>