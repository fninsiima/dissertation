#!/usr/bin/python
'''
Calculate the precision, recall and F-score of a particular whitefly detection
algorithm. Usage: options 

findaccuracy.py [-s|--show] 

options -s or --show display each test image, along with the ground truth, the
matches (true positives in blue, false positives in red), and a count of TP
(true positives), FP (false positives) and FN (false negatives). During image
display, press escape to quit from the image display, any other key to move to
the next image.

Without these options, the overall performance statistics are calculated with 
no images being displayed.
'''
import sys
import glob
import cv2
#import cv2.cv as cv
from parameters import par_dict
#import necrosismodule
#import necrosismodule2 #k=3
#import necrosismodule3 #k=2

import os
import numpy as np
import cv2
import cv2.cv as cv
import colorsys
import math
import time
eFactor = 1.15
import matplotlib.pyplot as plt
from skimage import data, io, filters, color, exposure
from skimage.feature import canny
import numpy as np
import math
import cv2
from scipy import ndimage as ndi
from skimage import morphology

#import clustering

#@profile
def pointDistance(P,Q):
    pass
    return np.linalg.norm([(p-q) for (p,q) in zip(P,Q)])
def display(image, msg=""):
    cv2.imshow(msg,image)
    cv2.waitKey(0)
def dot(image, center, color=(255,200,0), radius = 2):
    center = tuple(center)
    cv2.circle(image, center, radius, color, -1)
    return image

def slowestChange(array):
    if len(array)<3:
        print "slowestChange error"
        return 0, array[0]
    deriv1 = [float(array[i-1])/x for (i,x) in enumerate(array) if i>0]
    deriv2 = [abs(x-deriv1[i-1]) for (i,x) in enumerate(deriv1) if i>0]
    bestInd = deriv2.index(min(deriv2))+1
    return bestInd, array[bestInd]
def getLongestContour(image, t):
    imgray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    __,thresh = cv2.threshold(imgray,t,255,0)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    longestLen = 0
    longest = []
    for loop in contours:
        if len(loop) > longestLen:
            longestLen = len(loop)
            longest = loop
    return longest
def getBestContour(im, range_of_t=[5*t for t in range(4,45)]):
    areas = []
    contour_list = []

    for t in range_of_t:
        img = im.copy()
        longest = getLongestContour(im, t)

        if len(longest)==0:
            continue
        area = cv2.contourArea(longest)
        if area==0:
            continue
        areas.append(area)
        contour_list.append(longest)

        convex = cv2.convexHull(longest)
        # approx = cv2.approxPolyDP(convex,1/360.0*cv2.arcLength(convex,True),True)
        cv2.drawContours(img, [convex], -1, (0,200,255), 2)
        cv2.drawContours(img, [longest], -1, (200,0,255), 2)
        # cv2.imshow('t='+str(t),img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

    bestInd, _ = slowestChange(areas)
    bestCont = contour_list[bestInd]
    return range_of_t[bestInd], bestCont
def circleToContour(imsize,(x,y),r):
    # This is ugly as fuck
    getCircleImg = np.zeros(imsize,np.uint8)
    cv2.circle(getCircleImg,(x,y),r,(255,255,255),-1)
    grayCircleImg = cv2.cvtColor(getCircleImg,cv2.COLOR_BGR2GRAY)
    __, threshCircle = cv2.threshold(grayCircleImg, 127, 255, 0)
    contours, hierarchy = cv2.findContours(threshCircle,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    return contours[0]
def circleToRadialContour(imsize,(x,y),r,steps=360):
    maxY, maxX, __ = imsize
    maxY, maxX = maxY-1, maxX-1
    dt = 2*math.pi/steps
    def getEdgePixel(i):
        theta = dt*i
        dx = r*math.cos(theta)
        dy = r*math.sin(theta)
        return (sorted([int(x+dx), 0, maxX])[1], sorted([int(y+dy), 0, maxY])[1])
    return np.array([getEdgePixel(i) for i in range(steps)]).reshape((-1,1,2))
def getRegion(image, contour):
    # This function is somewhat inefficient
    # print "\nGathering region"
    rows,cols,c = image.shape
    indices = np.indices((cols,rows)).swapaxes(0,2)
    def isInside(indexCoords):
        xcoord = indexCoords[0]
        ycoord = indexCoords[1]
        return cv2.pointPolygonTest(contour,(xcoord,ycoord),False)==1
    boolCoords = np.apply_along_axis(isInside,2,indices)
    innerCoords = indices[boolCoords]
    colorCoords = image[boolCoords]
    return innerCoords, colorCoords
def nearestColor(center, validColors):
    distances = np.array([np.linalg.norm(center-c) for c in validColors])
    return min(zip(distances, validColors), key=lambda x: x[0])[1]
def replaceCenters(labels, centers, smallest=5):
    counts = np.bincount(labels)
    if not (False in counts>smallest):
        return False, centers
    validColors = centers[counts>smallest]
    newCenters = np.array([g if n>smallest else nearestColor(g,validColors) for (n,g) in zip(counts,centers)])
    return True, newCenters
def hls2bgr((h,l,s)):
    (r,g,b) = colorsys.hls_to_rgb(h/180.,l/256.,s/256.)
    return (int(b*256), int(g*256), int(r*256))
def bgr2hls((b,g,r)):
    (h,l,s) = colorsys.rgb_to_hls(r/256.,g/256.,b/256.)
    return (180*h,256*l,256*s)
def lab2bgr((L,a,b)):
    lab = np.array([L,a,b]).reshape(-1,1,3).astype(np.uint8)
    rgb = cv2.cvtColor(lab,cv2.COLOR_LAB2BGR).reshape((3,))
    return tuple(int(v) for v in rgb)
def replaceSmallClusters(focusColors, labels, centers, smallest=5):
    k = centers.shape[0]
    counts = np.bincount(labels[:,0])
    needsRepeat = (False in (counts>smallest).tolist())

    validColors = centers[counts>smallest]
    badLabels = np.arange(k)[counts<=smallest]
    newCenters = np.array([nearestColor(g,validColors) for (n,g) in zip(counts,centers) if not n>smallest])
    def replacePixels(colorlabel):
        color = colorlabel[0]
        label = colorlabel[1][0]
        if label in badLabels:
            return newCenters[badLabels==label]
        return color
    focusColors = np.apply_along_axis(replacePixels,1,zip(focusColors,labels))
    return needsRepeat, counts, focusColors
def getHLScoordinates(image,val=100):
    image = cv2.cvtColor(image,cv2.COLOR_BGR2HLS)
    def f(v):
        return np.array([v[0],val,v[2]])
    image = np.apply_along_axis(f,2,image)
    # Comment the following to cluster by HLS instead of BGR
    image = cv2.cvtColor(image.astype(np.uint8),cv2.COLOR_HLS2BGR)
    return image
def getLABcoordinates(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB).astype(np.uint16)
    return image
#def getKmeansClusters(k, focusColors, maxIterations=int(5/bigScale)):
#    needsRepeat = True
#    numIterations = 0
#    while needsRepeat and numIterations<maxIterations:
#        print "\nNew K-means Iteration"
#        __, labels, centers = cv2.kmeans(focusColors.astype("float32"), 
#                                    K = k, 
#                                    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 2, 15), 
#                                    attempts = 1, 
#                                    flags=cv2.KMEANS_RANDOM_CENTERS)
#        
#        numIterations+=1
#        needsRepeat, counts, focusColors = replaceSmallClusters(focusColors, labels, centers)
#        print "Counts :",counts
#        print "Centers:",centers
#
#    return labels, centers, focusColors
def getEMClusters(k, focusColors):
    # print "New EM Iteration"
    em = cv2.EM(k)
    __, LL, labels, probs = em.train(focusColors.astype("float32"))
    centers = em.getMat('means')
    # counts = np.bincount(labels[:,0])
    # print "Counts :",counts
    # print "Centers:\n",centers
    return LL, labels, probs, centers
def noSmallCentroids(image, k, focusColors, focusRegion):
    # labels, centers, focusColors = getKmeansClusters(k, focusColors)
    LL, labels, probs, centers = getEMClusters(k, focusColors)
    bgrcenters = [lab2bgr((l,a,b)) for (l,a,b) in centers]

    for i in range(1):#range(k):
        imcopy = image.copy()
        for (point, label) in zip(focusRegion, labels):
            label = label[0]
            pencolor = bgrcenters[label]
            # pencolor = red
            # if label!=i:
            #     continue
            (x,y) = tuple(int(c) for c in point)
            color = tuple(int(c) for c in centers[label])
            cv2.circle(imcopy,(x,y),0, pencolor)

        cv2.drawContours(imcopy, [focusCont], -1, (0,200,255),2)
        cv2.drawContours(im, [focusCont], -1, (0,200,255),2)

        cv2.imshow("Original",image)
        cv2.imshow('t='+str(bestThres)+', i='+str(i),imcopy)
        cv2.waitKey(0)    
def getMedianCenterColor(image,x,y,s=10):
    middleClump = image[(x-s):(x+s),(y-s):(y+s),:]
    # incomplete; abandoned
class bresenham:
    def __init__(self, image, start, end):
        self.start = list(start)
        self.end = list(end)
        self.length = pointDistance(self.start, self.end)
        self.center = list(start)
        self.image = image
        self.getPath()
        self.getPixels()    
    def getPath(self):
        self.path = []
        self.steep = abs(self.end[1]-self.start[1]) > abs(self.end[0]-self.start[0])
        if self.steep:
            self.start = self.swap(self.start[0],self.start[1])
            self.end = self.swap(self.end[0],self.end[1])
        
        if self.start[0] > self.end[0]:
            _x0 = int(self.start[0])
            _x1 = int(self.end[0])
            self.start[0] = _x1
            self.end[0] = _x0
            
            _y0 = int(self.start[1])
            _y1 = int(self.end[1])
            self.start[1] = _y1
            self.end[1] = _y0
        
        dx = self.end[0] - self.start[0]
        dy = abs(self.end[1] - self.start[1])
        error = 0
        derr = dy/float(dx)
        
        ystep = 0
        y = self.start[1]
        
        if self.start[1] < self.end[1]: ystep = 1
        else: ystep = -1
        
        for x in range(self.start[0],self.end[0]+1):
            if self.steep:
                self.path.append((y,x))
            else:
                self.path.append((x,y))
            
            error += derr
            
            if error >= 0.5:
                y += ystep
                error -= 1.0

        if list(self.path[0])==list(self.center):
            self.path.reverse()
    def getPixels(self):
        x_coords = [x for (y,x) in self.path]
        y_coords = [y for (y,x) in self.path]
        self.pixels = self.image[x_coords, y_coords, :]
    def swap(self,n1,n2):
        return [n2,n1]
def colorDiff(P, Q):
    pass
    return pointDistance(P,Q)**1.5
def findPixelJump(radius, r, winMin=0, winMax=-1, outer=None, inner=None, prevPoint = -1, eFactor=1.15, t=3.0):
    length = radius.length
    pixels = radius.pixels
    coords = radius.path
    if type(pixels)==type(np.array([])):
        pixels = pixels.tolist()
    # print "Warning: Old findPixelJump"
    k1 = 0        # error bound for eFactor
    k2 = 0.5    # percent of outside you want to restrict to; higher = more center
                # 0.7 works really well on image7, but screws everything else up
    N = len(pixels)
    if winMax == -1:
        winMax = int(N*k2)
    outerK = ((eFactor-1)*k1+1)/eFactor
    outerI = (1-float(r * eFactor * outerK)/length)*N
    diff = [colorDiff(pixels[i-1],pixels[i]) for (i,x) in enumerate(pixels) if i>0]
    def weightDist(i,x):
        if i<outerI:
             return 0
        angle_weight = 1
        dist_weight = 1
        color_sim = 1

        if not(outer is None or inner is None):
            outer_sim = similarity(outer, pixels[i-1])
            inner_sim = similarity(inner, pixels[i])
            color_sim = outer_sim * inner_sim
        if type(prevPoint) is not type(-1):
            # cos = cosWRTcenter(coords[-1],coords[i],prevPoint)
            # angle_weight = (1-cos*cos)**.5
            dist_weight = pointDistance(prevPoint, coords[i])**(-1)
        return x * dist_weight * color_sim * angle_weight
    dist = [weightDist(i,d) for (i,d) in enumerate(diff)]
    
    bestScore = max(dist[winMin:winMax])
    bestIndex = dist.index(bestScore)
    bestColorSim = bestScore
    bestDiff = diff[bestIndex]
    if type(prevPoint) is not type(-1):
        bestColorSim = bestScore*pointDistance(prevPoint, coords[bestIndex])

    # mu, sig = np.mean(dist), np.std(dist)
    # cumulZ = [(x-mu)/sig if sig!=0 else 0 for x in dist]
    # filtered = [int(abs(x)>t) for x in cumulZ]
    # bestIndex = filtered.index(1) if 1 in filtered else 0#int(outerI)

    return bestIndex, bestDiff
def similarity(P,Q):
    P = P/np.linalg.norm(P)
    Q = Q/np.linalg.norm(Q)
    dotprod = np.sum(P*Q)
    return dotprod
def cosWRTcenter(C, P, Q):
    C = np.array(C)
    P = np.array(P)
    Q = np.array(Q)
    dotprod = similarity(P-C,P-Q)
    # PC = (P-C)/np.linalg.norm(P-C)
    # PQ = (P-Q)/np.linalg.norm(P-Q)
    # dotprod = np.sum(PC*PQ)
    return dotprod
def cosine(C, P, Q):
    pass
    return cosWRTcenter(C,P,Q)
def getMedianIndex(edgePoints, center):
    edgePoints = edgePoints.reshape((-1,2))
    center = np.array(center)
    def getDist(point):
        return np.linalg.norm(point-center)
    pointDistances = np.apply_along_axis(getDist,1,edgePoints)
    if len(pointDistances)%2==0:
        pointDistances = np.delete(pointDistances,0,0)
        # make the length odd; doesn't really matter which point
    median = np.median(pointDistances)
    return np.where(pointDistances==median)[0][0]
def aS1(C, P, Q):
    max_cosine = 0.707
    cos = cosWRTcenter(C,P,Q)
    if abs(cos)<max_cosine:
        return True
    return False
def aS2(C,P,Q,s):
    margin_error = 0.03
    PQ = pointDistance(P,Q)/s
    CP = pointDistance(P,C)
    CQ = pointDistance(Q,C)
    if abs(2*PQ/(CP+CQ)) < margin_error:
         return True
    return False
def mediocreContour(C, contour, pm=3):
    (cx, cy) = tuple(C)
    shape = contour.shape
    contour = contour.reshape((-1,2))
    n = contour.shape[0]
    old_dist = [pointDistance(P,C) for P in contour]
    new_dist = [np.median(\
                    [pointDistance(P,C) for P in \
                    contour.take(range(2*(i-pm), 2*(i+pm+1)), mode='wrap').reshape((-1,2))]\
                ) for i in range(n)]
    scales = [new/old for (new, old) in zip(new_dist, old_dist)]
    new_contour = [(int(cx+(x-cx)*s), int(cy+(y-cy)*s)) for ((x,y),s) in zip(contour, scales)]
    return np.array(new_contour).reshape(shape)
def getWholeContour(radii, r, windowPercent = 0.1):
    # currently assumes starting point is known
    # currently assumes traversing CW is fine
    # currently assumes arbitrary value of windowPercent
    iPoint = radii[0]
    radNumPixels = [len(rad.path) for rad in radii]
    jumps = [findPixelJump(iPoint, r)[0]]
    scores = [findPixelJump(iPoint, r)[1]]
    outer_color = iPoint.pixels[jumps[0]]
    inner_color = iPoint.pixels[jumps[0]+1]

    for i in range(1, len(radii)):
        prevIndex = jumps[-1]
        prevFrac = float(prevIndex)/radNumPixels[i-1]
        prevPoint = radii[i-1].path[prevIndex]
        rad = radii[i]
        lower = max(int((prevFrac-windowPercent)*radNumPixels[i]),0)
        upper = min(int((prevFrac+windowPercent)*radNumPixels[i]),radNumPixels[i]-1)
        index, score = findPixelJump(rad, r,
                 lower, upper, outer_color, inner_color, prevPoint)
        jumps.append(index)
        scores.append(score)
        outer_color = rad.pixels[index]
        inner_color = rad.pixels[index+1]

        # image = dot(image, rad.path[lower], yellow)
        # image = dot(image, rad.path[upper], purple)
    return jumps, scores
def getBothContours(radii,r):
    jcw, scw = getWholeContour(radii, r)
    radii_rev = partialReverse(radii)
    jcc, scc = getWholeContour(radii_rev, r)
    jcc = partialReverse(jcc)
    scc = partialReverse(scc)

    jumps = []
    jumps += jcw[:2]
    for i in range(2,len(jcw)):
        prev = radii[i-2].path[jumps[-2]]
        curr = radii[i-1].path[jumps[-1]]
        nextcc = radii[i].path[jcc[i]]
        nextcw = radii[i].path[jcw[i]]

        scorecc = -scc[i]*cosine(prev,curr,nextcc)/pointDistance(curr, nextcc)**.5
        scorecw = -scw[i]*cosine(prev,curr,nextcw)/pointDistance(curr, nextcw)**.5
        if scorecw>=scorecc:
            jumps.append(jcw[i]) 
        else:
            jumps.append(jcc[i])
    
    #jumps = [jcw[i] if scw[i]>scc[i] else jcc[i] for i in range(len(jcw))]
    return jumps, jcc, jcw
def partialReverse(array):
    array = list(reversed(array))
    return array[-1:] + array[:-1]
def mostLikelyEdge(radii,r):
    scores = [findPixelJump(rad, r)[1] for rad in radii]
    bestScore = scores.index(max(scores))
    radii = radii[bestScore:] + radii[:bestScore]
    return radii
def evalThreeClusters(labels, probs, labcenters):
    counts = np.bincount(labels[:,0])
    centers = [lab2bgr((l,a,b)) for (l,a,b) in labcenters]
    stdev = [np.std(c)/np.mean(c) for c in centers]
    whiteInd = stdev.index(min(stdev))
    brownInd = stdev.index(max(stdev))
    otherInd = -(whiteInd + brownInd)%3

    whiteDist = similarity(labcenters[whiteInd], labcenters[otherInd])
    brownDist = similarity(labcenters[brownInd], labcenters[otherInd])
    print "White:",centers[whiteInd], "\tBrown:",centers[brownInd], "\tOther:",centers[otherInd]
    if whiteDist>brownDist:
        print "white Wins"
        return np.array([counts[whiteInd]+counts[otherInd], counts[brownInd]])
    else:
        print "brown Wins"
        return np.array([counts[whiteInd], counts[brownInd]+counts[otherInd]])
    # if np.std(centers[whiteInd])>np.std(centers[brownInd]):
    #     (whiteInd, brownInd) = (brownInd, whiteInd)

    def countOther(prob):
        maxInd = list(prob).index(max(list(prob)))
        if maxInd == whiteInd:
            return np.array([1.0,0.0])
        elif maxInd == brownInd:
            return np.array([0.0,1.0])
        else:
            not_whiteProb = -math.log(prob[whiteInd])
            not_brownProb = -math.log(prob[brownInd])
            newprob = np.array([not_brownProb, not_whiteProb])
            newprob = newprob/np.linalg.norm(newprob)
            return np.array([0.0,0.0])
            return newprob
    twoClusters = np.apply_along_axis(countOther,1,probs)
    twoCounts = np.sum(twoClusters, axis=0)
    return twoCounts

def analyze_image(path):#for pathNo in range(1):
    #root = cv2.imread('image_test.jpg')
    root = cv2.imread(path)
    root2 = root
    height, width = root.shape[:2]
    #root = cv2.imread('test4.jpg')#
    #root = cv2.imread('DSC_0237.JPG')
    root = cv2.resize(root,(200,200))
    rt=root.copy();imcopy=root.copy()
    #clstd_root = resized(root.copy())
    roots = rt[:,:,0]#image = data.root()#data.coins()
    
    elevation_map = filters.sobel(roots)#originally import filter and used filter.sobel
    markers = np.zeros_like(roots)
    markers[roots < 50] = 1
    markers[roots > 100] = 2
    
    segmentation = morphology.watershed(elevation_map, markers)
    
    segmentation = ndi.binary_fill_holes(segmentation - 1)
    
    cnt_img = segmentation.copy()
    #----------dtype for scikit numpy.float64//for cv2 numpy.uint8
    skel = np.array(cnt_img, dtype=np.uint8)
    contours, hierarchy = cv2.findContours(skel, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    #ls = []
    #cimg = np.ones(root.shape[:2], dtype="uint8") * 255
    #for cnt in contours:
        #area=cv2.contourArea(cnt)
        #print area,len(cnt)
        
    #cnt = max(contours,key=len)
    #print len(cnt)
    for cnt in contours:
        
        #print len(cnt)
        #print len(cnt)
        #print cv2.contourArea(cnt)
        
        area = cv2.contourArea(cnt)
        #print area
        if area < 2000:
            continue
        #if area < 2000 or area > 4000:
            
           
        if len(cnt) < 5:
            continue
        #cimg = np.zeros_like(root)
        #cv2.drawContours(cimg,[cnt],-1,color=0,thickness=-1)
        #print area
        #pxs = cimg[cimg == 0]
        #print pxs.dtype#uint8
        #print len( pxs)
        #clusterDraw(root[pxs[1],pxs[0]])
        #hmn = root[pxs[1],pxs[0]]
        #ellipse = cv2.fitEllipse(cnt)
        #cv2.ellipse(root, ellipse, (0,255,0), 2)
        lab_modified_im = getLABcoordinates(root)
        focusRegion,focusColor=getRegion(lab_modified_im,cnt)
        
        K = 2;#k1=k2=k3=40
        
        '''for kmeans'''
        #criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        #ret,labels,centres = cv2.kmeans(np.float32(focusColor),K,criteria,10,cv2.KMEANS_RANDOM_CENTERS)

        '''comment/uncomment for EM'''
        LL, labels, probs, centres = getEMClusters(K, focusColor)
            
        k = labels.reshape((-1, ))
        k1=np.bincount(k)[0]; k2= np.bincount(k)[1];# k3= np.bincount(k)[2]
        #print k1
        total=k1+k2#+k3
        #print total
        #print float(k1)/float(total)
        #print (float(k1)/float(total))*100
        k1=100*float(k1)/float(total);k2=100*float(k2)/float(total);#k3=100*float(k3)/float(total);
        k1 = str(math.floor(k1))+'%'
        k2 = str(math.floor(k2))+'%'
        
        bgrcenters = [lab2bgr((l,a,b)) for (l,a,b) in centres]

        pencolor0 = bgrcenters[0]
        pencolor1 = bgrcenters[1]


        def evalTwoClusters(p, q):
            stdevp = np.std(p)/np.mean(p)
            stdevq = np.std(q)/np.mean(q)
            return ([stdevp, stdevq])
        
        [p, q] = evalTwoClusters(pencolor0, pencolor1)

            
        
        if min([p, q]) == p:
            bgrcenters[0] = (198,193,179)
            bgrcenters[1] = (80,93,120)
            lowest = k2
        else:
            bgrcenters[1] = (198,193,179)
            bgrcenters[0] = (80,93,120)
            lowest = k1

    
        
        for (point, label) in zip(focusRegion, labels):
            label=label[0];pencolor = bgrcenters[label];(x,y) = tuple(int(c) for c in point)
            #print pencolor
            #pencolor = bgrcentres[label];        
            cv2.circle(imcopy,(x,y),0, pencolor)
    

    
        #cv2.drawContours(imcopy, [cnt], -1, (0,200,255),2)
        #x,y,w,h = cv2.boundingRect(cnt)
        #cv2.rectangle(imcopy,(x,y),(x+w,y+h),(0,255,0),2)
    
    cv2.putText(imcopy,'CBSD=%s' %(lowest),(1,21),cv2.FONT_HERSHEY_SIMPLEX,0.4,par_dict['PAR_TEXT_COLOR'])
    result_file = os.path.join(par_dict['PAR_RESULTS_DIR'], os.path.basename(path))[:-4] + '_analysed.jpg'
    #score = 'CBSD=%s' %(lowest)
    
    cv2.imwrite(result_file, cv2.resize(imcopy,(width,height)))

    binary = cv2.resize(imcopy,(width,height))
    #print binary.shape
    foo,foo2=[],[]
    #print'start'
    for i in range(height):
        for j in range(width):
            if binary[i,j].tolist() == [198,193,179]:
                foo.append((i,j))
            elif binary[i,j].tolist() == [80,93,120]:
                foo.append((i,j))
            else:
                foo2.append((i,j))
    #print len(foo)+len(foo2); print width*height
    return foo, foo2
      
    return (imcopy, result_file)





def get_resize_dimensions(side, image):
    # Get reduced dimensions that maintain aspect ratio of image
    r = float(side) / image.shape[1]
    dim = (side, int(image.shape[0] * r))
    return dim

def resize_images(img_path):
    # Resize image because skimage and generally the image processing will
    # take forever with large images
    img_file_name = os.path.basename(img_path)
    img_directory = os.path.dirname(img_path)   

    par_dict['PAR_RESULTS_DIR'] = os.path.join(img_directory, 'results/')
    # Create results directory if it is non-existent
    if not os.path.exists(par_dict['PAR_RESULTS_DIR']):
        os.mkdir(par_dict['PAR_RESULTS_DIR'])

    image = cv2.imread(img_path)
    h,w = image.shape[:2]
    if h > w and h > 600:
        dim = get_resize_dimensions(400, image)
        resized_img = cv2.resize(image, dim)
    elif w > h and w > 600:
        dim = get_resize_dimensions(600, image)
        resized_img = cv2.resize(image, dim)
    else:
        resized_img = image
    """added"""
    #resized_img = cv2.resize(image,(200,200))
    """"""
    resized_img_file = os.path.join(par_dict['PAR_RESULTS_DIR'], img_file_name)
    cv2.imwrite(resized_img_file, resized_img)
    return resized_img_file


if __name__ == '__main__':
    imgfiles = glob.glob(par_dict['PAR_IMG_SOURCE_DIR'] + '*.JPG')
    for image_file in imgfiles:
        resized_img = resize_images(image_file)
        final_img = analyze_image(resized_img)
        
        
