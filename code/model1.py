"""
CBSD - Detector

ver. 20160122
"""

import numpy as np
import cv2
import glob
import os
import matplotlib.pyplot as plt
from scipy import ndimage as ndi
from skimage import color, io
from skimage.draw import circle_perimeter
from skimage.filters import threshold_otsu, gaussian_filter
from skimage.morphology import closing, square
from skimage.segmentation import clear_border
from skimage.transform import resize
from skimage.measure import label, regionprops, profile_line
from parameters import par_dict
from skimage import img_as_ubyte
def get_filtered_regions(image):
	# Given an image get the possible locations for the roots
	# Not very accurate - needs more work
	# Get threshold from image
	imggray = color.rgb2gray(image)
	n = 20 # controls gaussian kernel used for filtering
	imggray = gaussian_filter(imggray, sigma=256/(4.*n), mode='reflect')
	cv2.imwrite('./gaussfill_%d.jpg'%(int(np.random.random() * 100)),img_as_ubyte(imggray))
	#print imggray.dtype, type(imggray)
	threshold_value = threshold_otsu(imggray)
	print('threshold value:', threshold_value)
	img_background = imggray > threshold_value
	cv2.imwrite('./otsuthresh_%d.jpg'%(int(np.random.random() * 100)),img_as_ubyte(img_background))

	# Close small islands in the image
	img_closed = closing(img_background, square(par_dict['PAR_CLOSING']))
	clear_border(img_closed) # root with edges on border of image aren't detected

	# Label roots in image
	img_labelled = label(img_closed)
	cv2.imwrite('./imgclosed_%d.jpg'%(int(np.random.random() * 100)),img_as_ubyte(img_closed))
	# Get the different regions
	regions = regionprops(img_labelled)
	filtered_regions = [r for r in regions if r.area > par_dict['PAR_AREA_FILTER']]
	#print type(filtered_regions)
	return filtered_regions

#different from original ppd code
def process_image(image, root):
	# Process single root
	# Output root and maximum and minimum values
	img_int = root.astype(np.int)
	img_int = ndi.binary_erosion(img_int, structure = np.ones((10,10))).astype(img_int.dtype) # cut out edge of root
	img_int = closing(img_int, square(par_dict['PAR_CBSD_CLOSING']))

	# Create a smaller version of the image by erosion
	# This is to create a ring around the image where we can analyse it
	# img_int_b = ndi.binary_erosion(img_int, structure = np.ones(PAR_EROSION)).astype(img_int.dtype)
	
	# # Calculate a mask for the region
	# img_temp = np.ones_like(img_int_b)
	# img_mask = img_temp - img_int_b
	img_mask = np.logical_and(img_int, img_int)

	# from skimage import img_as_ubyte	
	cv2.imwrite('./maskoutsha_%d.jpg'%(int(np.random.random() * 100)), img_as_ubyte(img_mask))
	cv2.imwrite('./intoutsha_%d.jpg'%(int(np.random.random() * 100)), img_int)
	
	# Overlay mask on original image
	img = np.zeros_like(image)
	img[:,:,0] = img_mask
	img[:,:,1] = img_mask
	img[:,:,2] = img_mask
	img = image * img
	io.imsave('./cutoutsha_%d.jpg'%(int(np.random.random() * 100)), img)
	return img

def calculate_int_score(int_vals):
	# Calculate intensity average score
	val_nonzero = np.nonzero(int_vals)
	vals = int_vals[val_nonzero]
	return np.mean(vals)

#different from original ppd function
def calculate_scores_img(image, total_area):
	# Calculate scores around the circumference of image in the region of interest
	
	# Get size of image
	(ylen, xlen, dim) = image.shape # colored image
	
	NUM_INSPECTION_POINTS = ylen
	s = np.zeros((NUM_INSPECTION_POINTS))

	# Run intensity lines across the length of the image
	for i in range(NUM_INSPECTION_POINTS):
		intensity_line = profile_line(image, (0,i), (xlen,i), 1)
		red_profile = intensity_line[:,0]
		s[i] = sum(red_profile > par_dict['PAR_INTENSITY_THRESHOLD'])
	
	ppd_score = sum(s)/total_area 
	
	return ppd_score

##ADDED new way to calculate ppd_score
def calculate_scores_img2(image, total_area):
	# Find the number of pixels in the image that were assigned RED value = [255,0,0]
    # since RED pixels represent PPD
	color = [255,0,0];
 
	indices = np.where(np.all(image == color, axis=-1))	
	foo= zip(indices[0], indices[1]);
 
	# Number of pixels = len(foo)   
	ppd_score = len(foo)/float(total_area);     
 
	return ppd_score

def get_ppd_area(processed_image):
	# io.imsave('./procoutsha_%d.jpg'%(int(np.random.random() * 100)), processed_image)
        
	# Get the region that is covered by PPD
	# image must be a grayscale image
	# returns a mask of the PPD area based on empirical thresholds
#     imgt1 = processed_image < 150 
#     imgt2 =  processed_image > 20
	imgt1 = processed_image < par_dict['PAR_PPD_UPPER_THRESHOLD']
	imgt2 =  processed_image > par_dict['PAR_PPD_LOWER_THRESHOLD']
	imgt = imgt1 * imgt2 # get intersection of two filtered images
	io.imsave('./cbsdmask_%d.jpg'%(int(np.random.random() * 100)), img_as_ubyte(imgt))
	return imgt

def get_resize_dimensions(side, image):
	# Get reduced dimensions that maintain aspect ratio of image
	r = float(side) / image.shape[1]
	dim = (side, int(image.shape[0] * r))
	return dim

def resize_images(img_path):
	# Resize image because skimage and generally the image processing will
	# take forever with large images
	img_file_name = os.path.basename(img_path)
	img_directory = os.path.dirname(img_path)   

	par_dict['PAR_RESULTS_DIR'] = os.path.join(img_directory, 'results/')
	# Create results directory if it is non-existent
	if not os.path.exists(par_dict['PAR_RESULTS_DIR']):
		os.mkdir(par_dict['PAR_RESULTS_DIR'])

	image = cv2.imread(img_path)
	h,w = image.shape[:2]
	if h > w and h > 600:
		dim = get_resize_dimensions(400, image)
		resized_img = cv2.resize(image, dim)
	elif w > h and w > 600:
		dim = get_resize_dimensions(600, image)
		resized_img = cv2.resize(image, dim)
	else:
		resized_img = image

	resized_img_file = os.path.join(par_dict['PAR_RESULTS_DIR'], img_file_name)
	cv2.imwrite(resized_img_file, resized_img)
	return resized_img_file

	
def analyze_image(image_file):
	# Main PPD analysis file
	# Saves file in results folder after processing

	img = ndi.imread(image_file)
	orig = img.copy()
	filtered_regions = get_filtered_regions(img)
	num_regions = len(filtered_regions);#print num_regions
	NUM_ROOTS_PER_IMG = num_regions if num_regions < 7 else par_dict['PAR_NUM_ROOTS_PER_IMG']
	root_scores = 0.0
	pixels = 0.0
	##ADDED check if atleast one root is found
	if NUM_ROOTS_PER_IMG != 0: 
        	#print('rootsfound')
        	for i in range(NUM_ROOTS_PER_IMG):
        		root = filtered_regions[i]
        		[mr, mc, maxr, maxc] = root.bbox
          
        		##ADDED draw a GREEN rectangle around root in the final image returned...
        		##... to see what exactly was processed 
        		##... because in some cases it wasnt necessarily procesing a root 
        		##... but some random object in the background          
        		cv2.rectangle(img,(mc,mr),(maxc,maxr),[0,255,0])
        		#cv2.imwrite('filteredroot.jpg',cv2.cvtColor(img,cv2.COLOR_RGB2BGR))
        		img_root = img[mr:maxr, mc:maxc]
        		img_root_orig = np.copy(img_root)
              ##CHANGED from root.image to root.filled_image
        		#cv2.imwrite('rootfiltered.jpg', img_as_ubyte(root.filled_image))
        		img_processed = process_image(img_root, root.filled_image) 
        		img_processed_gray = color.rgb2gray(img_processed)
        		
        		PPD_mask = get_ppd_area(img_processed_gray)
        		img_root_orig = color.gray2rgb(img_root_orig)
        		img_root_orig[PPD_mask] = [255,0,0] # color identified PPD as red for score calculation
          
        		## CHANGED wrote a new function to calculate ppd_score...
        		## ...called calculate_scores_img2()
        		"""
        		previously this was calculated by passing a scan / intensity line throughout the image
        		and assessing whether the RED value of the pixels along a scan line
        		are greater than PAR_INTENSITY_THRESHOLD = 254.0   
        		and summing them up          
          
        		this logic was challenging because
        		1) it didnt consider the values of the GREEN and RED components
        		2) ppd pixels have been assigned a value of [255,0,0] but this method counts
        		   even those pixels which are say [255,67,98]
                therefore as long as the RED value is 255, regardless of whether it is
                a ppd_pixel or not, it is counted
        		"""          
        		ppd_score = calculate_scores_img2(img_root_orig, root.filled_area) ##CHANGED from root.area to root.filled_area
        		pixels += root.area  
        		img_root_orig[PPD_mask] = par_dict['PAR_PPD_ANOTATION_COLOR'] # color identified PPD for visual purposes
        		root_scores += ppd_score
        		# cv2.putText(img_root_orig, 'AR=%2.2f%%'%(ppd_score), (int(img_root_orig.shape[0]/4),int(img_root_orig.shape[1]/2)),cv2.FONT_HERSHEY_SIMPLEX,0.5,PAR_TEXT_COLOR)
        
        		img[mr:maxr, mc:maxc] = img_root_orig
        		result_file = image_file[:-4] + '_analysed.jpg'

        	"""ADDED"""
        	c = par_dict['PAR_PPD_ANOTATION_COLOR'];
        	indices = np.where(np.all(img == c, axis=-1))
        	foo= zip(indices[0], indices[1]);
        	cbsd_area = root_scores/NUM_ROOTS_PER_IMG
   
        	#cv2.putText(img, 'CBSD=%2.2f %%'%((root_scores/NUM_ROOTS_PER_IMG)*100), (int(img.shape[0]/10),int(img.shape[1]/15)),cv2.FONT_HERSHEY_SIMPLEX,0.8,par_dict['PAR_TEXT_COLOR'])
        	#io.imsave(result_file, img)
        	return foo, orig, cbsd_area, pixels

    ##ADDED another way of segmenting out roots
    ##...based on Aarons method of clustering
    ##...gets the bestContour and returns its bounding box coordinates
    ##...still not 100% perfect 
    ##...Draws an ORANGE bounding box to differentiate it from the previous otsu method
    ##...which instead draws an GREEN bounding box
	elif NUM_ROOTS_PER_IMG == 0:
            #print('toobad')
            ##ADDED Aarons module
            ##... too big a file on its own to add all the functions
            ##... within this analyzer file
            import clustering
            
            def get_filtered_regions2(path,cnt):
                image=cv2.imread(path)
                # draw mask
                mask = np.full((image.shape[0], image.shape[1]), 0, dtype=np.uint8) 
                cv2.drawContours(mask, [cnt], -1, 255, -1) #previously 0 not 255
                image[mask==0] = [0, 0, 0]
                #cv2.imwrite('../mask_%d.jpg'%(int(np.random.random() * 100)),mask)
                return image,mask
            
            ## Sometimes Clustering.py returns exceptions
            ## havent yet debugged this issue
            try:
                ## get the bounding box and the bestContour
                cnt,box=clustering.analyze_image(image_file)                
            except:
                ## return 'nan %' ppd_score                
                result_file = image_file[:-4] + '_analysed.jpg'
                ppd_score = 'nan %'     
                #cv2.putText(img, 'CBSD=%s'%(ppd_score), (int(img.shape[0]/10),int(img.shape[1]/15)),cv2.FONT_HERSHEY_SIMPLEX,0.8,par_dict['PAR_TEXT_COLOR'])          	
                #cv2.imwrite(result_file, cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
                return [], orig, 'no root found', None  
            else:
                ## do the analysis incase of no exception  
              
                # get shape of original image
                h,w=img.shape[:2] 
                
                # get coords of bounding box                          
                mr = min(box[:,1]); mc=min(box[:,0]); maxr=max(box[:,1]); maxc=max(box[:,0])
                
                # sometimes the corners of the bounding box exceed the image dimensions
                # set to the min = 0 and max = h or w depending.
                
                ##CHANGED
                if mr < 0:
                    mr = 0
                if mc < 0:
                    mc = 0
                if maxr > h:
                    maxr = h - 1
                if maxc > w:
                    maxc = w - 1  
                
                # new way to get_filtered_region
                filtered_region, mask = get_filtered_regions2(image_file,cnt)
                new_mask = mask[mr:maxr, mc:maxc];  
                ##REMOVED
                ##necro_area = cv2.countNonZero(new_mask) # replaces root.filled_area when calculating ppd_score
                
                img_root = img[mr:maxr, mc:maxc]
                img_root_orig = np.copy(img_root);                   
                try:
                    # can return exception sometimes in this clustering case
                    # havent debugged yet
                    img_processed = process_image(img_root,new_mask)
                except:
                    # return 'nan %' score
                    result_file = image_file[:-4] + '_analysed.jpg'             
                    score = 'nan %'     
                    cv2.putText(img, 'CBSD=%s'%(score), (int(img.shape[0]/10),int(img.shape[1]/15)),cv2.FONT_HERSHEY_SIMPLEX,0.8,par_dict['PAR_TEXT_COLOR'])      	
                    cv2.imwrite(result_file, cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
                    return [], orig, 'no root found', None         
                else: 
                    ##ADDED new way to process the area
                    h,w=img_processed.shape[:2]
                    c = [0,0,0];indices = np.where(np.all(img_processed == c, axis=-1))
                    foo= zip(indices[0], indices[1]);#print img_root.shape#print len(foo);    
                    necro_area = h*w - len(foo);
                    
                    img_processed_gray = color.rgb2gray(img_processed);             
                    PPD_mask = get_ppd_area(img_processed_gray)
                    img_root_orig = color.gray2rgb(img_root_orig);
                    img_root_orig[PPD_mask] = [255,0,0] # color identified PPD as red for score calculation
                    ##ADDED calculate ppd_score using new function
                    ppd_score = calculate_scores_img2(img_root_orig, necro_area) 
                    pixels += necro_area
                    img_root_orig[PPD_mask] = par_dict['PAR_PPD_ANOTATION_COLOR']
                    img[mr:maxr, mc:maxc]=img_root_orig   
                    ##ADDED draw ORANGE bounding rect
                    cv2.drawContours(img,[box],-1, (255,200,0), 2)                      
                    result_file = image_file[:-4] + '_analysed.jpg'              
                    score = '%2.2f'%((ppd_score/1.0)*100)      
                    #cv2.putText(img, 'CBSD=%2.2f %%'%((ppd_score/1.0)*100), (int(img.shape[0]/10),int(img.shape[1]/15)),cv2.FONT_HERSHEY_SIMPLEX,0.8,par_dict['PAR_TEXT_COLOR'])                    
                    #cv2.imwrite(result_file, cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
                    
                    """ADDED"""
                    c = par_dict['PAR_PPD_ANOTATION_COLOR'];
                    indices = np.where(np.all(img == c, axis=-1))
                    foo= zip(indices[0], indices[1]);
                    cbsd_area = ppd_score#root_scores/NUM_ROOTS_PER_IMG                    
                    return foo, orig, cbsd_area, pixels

	##ADDED return 'nan %' as the ppd_score if no root was processed
	else:
        	result_file = image_file[:-4] + '_analysed.jpg'         
        	ppd_score = 'nan %'     
        	#cv2.putText(img, 'CBSD=%s'%(ppd_score), (int(img.shape[0]/10),int(img.shape[1]/15)),cv2.FONT_HERSHEY_SIMPLEX,0.8,par_dict['PAR_TEXT_COLOR'])
        	#cv2.imwrite(result_file, cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
        	return [], orig, 'no root found', None        

if __name__ == '__main__':
	imgfiles = glob.glob(par_dict['PAR_IMG_SOURCE_DIR'] + '*.JPG')
	for image_file in imgfiles:
		resized_img = resize_images(image_file)
		final_img = analyze_image(resized_img)



